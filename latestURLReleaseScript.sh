artifactory_url="http://127.0.0.1:8081/artifactory"

repo=libs-snapshot-local
artifact=com/agiletestingalliance/cpdofsep/CPDOFSep21ONE
artifactApp=CPDOFSep21ONE
name=$artifact

url="$artifactory_url/$repo/$artifact"  ## genrate complete artifact url/path to where build resides.
echo "URL: " + $url

version=`curl -s $url/maven-metadata.xml | grep latest | grep -Po '>\K[^<]*'`

echo "version: = " + $version

build=`curl -s $url/$version/maven-metadata.xml | grep '<value>' | head -1 | grep -Po '>\K[^<]*'`


curl -s $url/1.0-SNAPSHOT/maven-metadata.xml

echo "build: = " $build


BUILD_LATEST="$url/$version/$artifactApp-$build.war"   ## Storing latest build (war) value into "BUILD_LATEST" variable.

echo $BUILD_LATEST > filename.txt
